#!/bin/bash
#
# Cheap lil battery monitor to log hourly changes
# righttoprivacy@tutanota.com
#

batlog="/home/mobian/bat.log"   # location for log
slptime="60m"                   # time between log
echo "Battery Monitor" >> $batlog

while :
do
    echo '__________________' >> $batlog
    echo "$slptime"
    upower -d | grep percentage | awk {'print $2'}|head -n 1 >> $batlog
    sleep $slptime
 done   
